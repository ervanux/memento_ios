//
//  ViewController.swift
//  memento
//
//  Created by Erkan Ugurlu on 12/07/15.
//  Copyright (c) 2015 LameCoders. All rights reserved.
//

import UIKit
import Parse

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var passwordSecond: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     }

    @IBAction func nextButtonPressed(sender: AnyObject) {
        if (isStringValid(username.text)
            && isStringValid(password.text)
            && password.text == passwordSecond.text) {

                let user = PFUser()
                user.username = username.text
                user.password = password.text
                showLoading()
                user.signUpInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
                    self.hideLoading()
                    if error == nil {
                        log.info("user saved successfully")
//                        self.performSegueWithIdentifier("showSuccessRegistration", sender: self)
                        self.dismissViewControllerAnimated(true, completion: nil)
                    } else {
                        self.showErrorPopup("Niye bilmiyorum kayıt olmadı.")
                    }
                }
        } else {
            showErrorPopup("Eksik bişey mi var?")
        }
        
    }
    
}

