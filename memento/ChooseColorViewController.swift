//
//  ChooseColorViewController.swift
//  memento
//
//  Created by Erkan Ugurlu on 14/07/15.
//  Copyright (c) 2015 LameCoders. All rights reserved.
//

import UIKit

class ChooseColorViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var selectedItemIndexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView.scrollToItemAtIndexPath(self.selectedItemIndexPath, atScrollPosition: UICollectionViewScrollPosition.CenteredVertically, animated: true)
        
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) 
        cell.backgroundColor = UIColor(array[indexPath.row])
        if indexPath == self.selectedItemIndexPath {
            cell.layer.borderColor = UIColor.blackColor().CGColor
            cell.layer.borderWidth = 4.0
        } else {
            cell.layer.borderColor = nil
            cell.layer.borderWidth = 0.0
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var indexPaths: Array = [indexPath]
        //        if self.selectedItemIndexPath != nil {
        if indexPath == self.selectedItemIndexPath {
            //                self.selectedItemIndexPath = nil
        } else {
            indexPaths.append(self.selectedItemIndexPath)
            self.selectedItemIndexPath = indexPath
        }
        //        } else {
        //            self.selectedItemIndexPath = indexPath
        //        }
        collectionView.reloadItemsAtIndexPaths(indexPaths)
    }


    @IBAction func pressedSaveButton(sender: AnyObject) {
        self.performSegueWithIdentifier("backToCategory", sender: nil)
    }
    
}
