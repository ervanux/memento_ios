//
//  UIView.swift
//  memento
//
//  Created by Erkan Ugurlu on 20/09/15.
//  Copyright © 2015 LameCoders. All rights reserved.
//

import UIKit

extension UIView {
    func setMask(corners:UIRectCorner){
        let rounded = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners,cornerRadii: CGSize(width: 5.0,height: 5.0))
        let shape = CAShapeLayer()
        shape.path = rounded.CGPath
        self.layer.mask = shape;
    }
}


