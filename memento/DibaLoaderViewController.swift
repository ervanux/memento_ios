//
//  DibaLoaderViewController.swift
//  INGDiBa
//
//  Created by Erkan Ugurlu [Mobil Yazilim  Servisi] on 18/09/15.
//  Copyright (c) 2015 INGBank Turkiye. All rights reserved.
//

import UIKit
import QuartzCore

class DibaLoaderViewController: UIViewController {
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var indicatorBack: UIView!
    @IBOutlet weak var label: UILabel!

    override func loadView() {
        super.loadView()

        self.indicatorBack.layer.cornerRadius = 7
        self.indicatorBack.layer.borderWidth = CGFloat(0.5)
        self.indicatorBack.layer.borderColor = UIColor.lightGrayColor().CGColor
        
//        self.label.preferredMaxLayoutWidth = 150
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.indicator.startAnimating()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        self.indicator.stopAnimating()
    }
    
    func hideAll(){
        self.view.alpha = 0
    }
    
    func showAll(){
        self.view.alpha = 1
    }
}


public class DibaLoader {
    static var loaderController:DibaLoaderViewController!
    
    public class func show(title title: String?) {
        let lockQueue = dispatch_queue_create("com.diba.showlock", nil)
        dispatch_sync(lockQueue) {
            if DibaLoader.loaderController == nil {
                DibaLoader.loaderController = UIStoryboard(name: "Components", bundle: nil).instantiateViewControllerWithIdentifier("LoaderViewController") as! DibaLoaderViewController
                //            currentWindow.rootViewController?.addChildViewController(loaderController)
            }
            
            if let label = DibaLoader.loaderController.label {
                if let title = title {
                    label.text = title
                } else {
                    label.text = ""
                }
            }
            
            if DibaLoader.loaderController.view.superview == nil {
                let currentWindow : UIWindow = UIApplication.sharedApplication().keyWindow!
                
//                let height : CGFloat = UIScreen.mainScreen().bounds.size.height
//                let width : CGFloat = UIScreen.mainScreen().bounds.size.width
//                let center : CGPoint = CGPointMake(width / 2.0, height / 2.0)
                DibaLoader.loaderController.view.frame = UIScreen.mainScreen().bounds
                
                dispatch_async(dispatch_get_main_queue(),{
                    currentWindow.addSubview(DibaLoader.loaderController.view)
                    UIView.animateWithDuration(0.3, animations: { () -> Void in
                        DibaLoader.loaderController.showAll()
                        }, completion:nil)
                })
            }
        }

    }
    
    public class func hide(){
        let lockQueue = dispatch_queue_create("com.diba.hidelock", nil)
        dispatch_sync(lockQueue) {
            if DibaLoader.loaderController != nil && DibaLoader.loaderController.view.superview != nil {
                dispatch_async(dispatch_get_main_queue(),{
                    UIView.animateWithDuration(0.3, animations: { () -> Void in
                        DibaLoader.loaderController.hideAll()
                        }, completion: { (_) -> Void in
                            DibaLoader.loaderController.view.removeFromSuperview()
                    })
                    
                })
            }
        }

    }

}


