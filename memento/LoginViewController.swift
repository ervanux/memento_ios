//
//  LoginViewController.swift
//  memento
//
//  Created by Erkan Ugurlu on 12/07/15.
//  Copyright (c) 2015 LameCoders. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var backView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let _ = PFUser.currentUser() {
            self.showLoading()
            self.performSegueWithIdentifier("showDashboardWithoutAnimate", sender: nil)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.username.text = ""
        self.password.text = ""
        self.username.becomeFirstResponder()
    }
    
    @IBAction func pressedBackButton(sender: AnyObject) {
        self.login(self.username.text!, password: self.password.text!)
    }
    
    
    func login(username:String, password: String){
        if isStringValid(username) && isStringValid(password) {
            showLoading()
            
            PFUser.logInWithUsernameInBackground(username, password:password) {
                (user: PFUser?, error: NSError?) -> Void in
//                do{
//                    try Locksmith.saveData(["username":username,"password":password], forUserAccount: "Account")
//                }catch {
//                    
//                }
                self.hideLoading()
                if user != nil {
                    self.performSegueWithIdentifier("showDashboard", sender: nil)
                } else {
                    // The login failed. Check error to see why.
                    self.showErrorPopup("Olmadı giremedim.")
                    
                }
            }
        }

    }
    
    @IBAction func unwindFromRegister(sende:UIStoryboardSegue){
    
    }

    
}
