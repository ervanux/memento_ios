//
//  AddNewCategoryViewController.swift
//  memento
//
//  Created by Erkan Ugurlu on 12/07/15.
//  Copyright (c) 2015 LameCoders. All rights reserved.
//

import UIKit
import Parse
import QuartzCore

class AddNewCategoryViewController: UIViewController {
    var selectedRandomColorIndex:Int!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var categoryName: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var selectedColorButton: UIButton!
    
    @IBOutlet weak var valueTemplateName: UITextField!
    @IBOutlet weak var valueTemplateType: UISegmentedControl!
    
    var valueTemplates  = [ValueTemplate]()
    
    @IBAction func pressedAddNewValueTemplate(sender: AnyObject) {
        if isStringValid(valueTemplateName.text) {
            self.valueTemplates.append(ValueTemplate(name: valueTemplateName.text!, typ: ValueTypes(rawValue: self.valueTemplateType.selectedSegmentIndex)!))
            self.tableView.reloadData()
            self.valueTemplateName.text = ""
            self.valueTemplateType.selectedSegmentIndex = 0
        }
    }
    
    @IBAction func pressedSave(sender: AnyObject) {
        if isStringValid(categoryName.text){
            let category = PFObject(className:"Category")
            category["user"] = PFUser.currentUser()
            category["name"] = categoryName.text
            category["colorCode"] = self.selectedColorButton.backgroundColor?.hexString
            category["order"] = 1
            
            //            self.showLoading()
            //            category.saveEventually({ (result:Bool, error:NSError?) -> Void in
            self.showLoading()
            category.saveInBackgroundWithBlock({ (result:Bool, error:NSError?) -> Void in
                if let error = error {
                    self.hideLoading()
                    log.error("ERROR: Save category : \(error)")
                } else {
                    PFUser.currentUser()?.relationForKey("categories").addObject(category)
                    PFUser.currentUser()?.saveInBackgroundWithBlock({ (result:Bool, error:NSError?) -> Void in
                        self.hideLoading()
                        for valueTemplate in self.valueTemplates {
                            let pfvalTemplate = PFObject(className:"EventDetailTemplate")
                            pfvalTemplate["category"] = category
                            pfvalTemplate["name"] = valueTemplate.name
                            pfvalTemplate["type"] = valueTemplate.typ.rawValue
                            pfvalTemplate.saveEventually({ (result:Bool, error:NSError?) -> Void in
                                if let error = error {
                                    log.error("ERROR: Save template : \(error)")
                                } else {
                                    category.relationForKey("detailTemplates").addObject(pfvalTemplate)
                                    category.saveEventually()
                                }
                            })
                        }
                        self.performSegueWithIdentifier("backToDashboard", sender: nil)
                    })
                    
                }
                
                
            })
            //            category.saveEventually({ (bool , error) -> Void in
            
            //                var array = [PFObject]()
            
            
            
            //                PFUser.currentUser()?.relationForKey("categories").addObject(category)
            //                if !array.isEmpty {
            //                    PFObject.saveAllInBackground(array, block: { (bool , error) -> Void in
            //                        for template in array {
            //                            category.relationForKey("detailTemplates").addObject(template)
            //                        }
            //                        category.saveEventually()
            //                        self.okJump(bool)
            //                    })
            //                } else {
            //                    self.okJump(true)
            //                }
            //            })
        }
    }
    
    func okJump(result:Bool){
        self.hideLoading()
        if result {
            self.performSegueWithIdentifier("backToDashboard", sender: nil)
        } else {
            self.showErrorPopup("Kayıt sırasında hata oluştu.")
        }
        
    }
    
    @IBAction func unwindFromColorChoose(segue:UIStoryboardSegue){
        if let vc = segue.sourceViewController as? ChooseColorViewController {
            self.selectedColorButton.backgroundColor = UIColor(array[vc.selectedItemIndexPath.row])
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addButton.layer.borderWidth = 1.0
        self.addButton.layer.borderColor = self.addButton.tintColor.CGColor
        self.addButton.layer.cornerRadius = 5.0
        
        self.categoryName.becomeFirstResponder()
        self.saveButton.enabled = false
        
        self.selectedRandomColorIndex = Int(arc4random_uniform(UInt32(array.count)))
        self.selectedColorButton.backgroundColor = UIColor(array[self.selectedRandomColorIndex])
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let nav = segue.destinationViewController as? UINavigationController, cont = nav.viewControllers.first as? ChooseColorViewController {
            cont.selectedItemIndexPath = NSIndexPath(forItem: self.selectedRandomColorIndex, inSection: 0)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.categoryName.becomeFirstResponder()
    }
    
    @IBAction func textChanged(sender: AnyObject) {
        self.changeSaveBtnStatus()
    }
    
}

enum ValueTypes:Int {
    case Yazi = 0,Sayi,Para
}

func convertTypetoString(type:ValueTypes)-> String{
    switch type {
    case .Yazi:
        return "Yazi"
    case .Sayi:
        return "Sayi"
    case .Para:
        return "Para"
    }
}

struct ValueTemplate{
    var name:String
    var typ:ValueTypes
    
    init(name:String, typ:ValueTypes) {
        self.name = name
        self.typ = typ
    }
}

extension AddNewCategoryViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return valueTemplates.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CELL")!
        cell.textLabel?.text = self.valueTemplates[indexPath.row].name
        cell.detailTextLabel?.text = convertTypetoString(self.valueTemplates[indexPath.row].typ)
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func changeSaveBtnStatus() {
        if let text1 = self.categoryName.text where text1.characters.count>0 {
            self.saveButton.enabled = true
        } else {
            self.saveButton.enabled = false
        }
    }
}


