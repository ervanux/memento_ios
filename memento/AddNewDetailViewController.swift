//
//  AddNewDetailViewController.swift
//  memento
//
//  Created by Erkan Ugurlu on 09/08/15.
//  Copyright (c) 2015 LameCoders. All rights reserved.
//

import UIKit
import Parse

class TemplateCell: UITableViewCell{
    @IBOutlet weak var name:UILabel!
    @IBOutlet weak var value:UITextField!
}

class AddNewDetailViewController: UIViewController {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var backView: UIView!
    
    var category:PFObject!
    var event:PFObject!
    var detailTemplates = [PFObject]()
    
    var finishedTaskCounter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addEvent()
        self.getTemplates()
        
        self.backView.backgroundColor = UIColor(hexString: self.category["colorCode"] as! String)
    }
    
    func getTemplates(){
        self.findObjects(self.category!, relationKey: "detailTemplates", completion: { (objects:[PFObject]?) -> Void in
            if let objects = objects  {
                for object in objects {
                    self.detailTemplates.append(object)
                }
                self.loadTableView()
            }
        })
    }

    func addEvent (){
        let event = PFObject(className: "Event")
        event["category"] = category
        //        event.saveEventually { (result, error) -> Void in
        event.saveInBackgroundWithBlock({ (result, error) -> Void in
            if let error = error {
                log.error("ERROR: Save category : \(error)")
            } else {
                self.event = event
                self.category.relationForKey("events").addObject(event)
                self.category.saveEventually()
                self.loadTableView()
            }
        })

    }
    
    func loadTableView() {
        if ++finishedTaskCounter == 2 {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.finishedTaskCounter = 0
                if let cdate = self.event.createdAt {
                    self.headerLabel.text = "\(dateformatterDate(cdate)) tarihli olay eklendi. \n Aşağıdan detaylarını girebilirsin."
                    self.tableView.reloadData()
                }
                //                self.hideLoading()
            })
        }
    }
    
    @IBAction func pressedSave(sender: AnyObject) {
//        var eventDetails = [PFObject]()
        for rowIndex in 0..<self.detailTemplates.count {
            let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: rowIndex, inSection: 0)) as! TemplateCell
            let eventDetail = PFObject(className: "EventDetail")
            eventDetail["template"] = self.detailTemplates[rowIndex]
            eventDetail["value"] = cell.value.text
            eventDetail["event"] = self.event
//            eventDetails.append(eventDetail)
            eventDetail.saveEventually({ (result:Bool, error:NSError?) -> Void in
                if let error = error {
                    log.error("ERROR: Save detail : \(error)")
                } else {
                    self.event.relationForKey("details").addObject(eventDetail)
                    self.event.saveEventually()
                }
            })
        }

        self.performSegueWithIdentifier("returnDasboarAfterAdding", sender: nil)
        
//        if !eventDetails.isEmpty {
//            self.showLoading()
            
//            PFObject.saveAllInBackground(eventDetails, block: { (result, error) -> Void in
//                for detail in eventDetails {
//                    self.event.relationForKey("details").addObject(detail)
//                }
//                self.event.saveEventually()
//                self.hideLoading()
//                if result {
//                    self.performSegueWithIdentifier("returnDasboarAfterAdding", sender: nil)
//                } else if let _ = error {
//                    self.showErrorPopup("Kayıt edemedim")
//                }
            
//            })
//        }
    }
    
    @IBAction func pressedCancel(sender: AnyObject) {
        self.performSegueWithIdentifier("returnDasboarAfterAdding", sender: nil)
    }
    
}

func dateformatterDate(date: NSDate) -> NSString{
    let dateFormatter: NSDateFormatter = NSDateFormatter()
    dateFormatter.dateStyle = .FullStyle
    dateFormatter.timeStyle = .MediumStyle
    dateFormatter.locale = NSLocale(localeIdentifier: "Tr_tr")
    
    return dateFormatter.stringFromDate(date)
}


extension AddNewDetailViewController: UITableViewDataSource{
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell1") as! TemplateCell
        cell.name.text = self.detailTemplates[indexPath.row]["name"] as? String
        if ValueTypes(rawValue: self.detailTemplates[indexPath.row]["type"] as! Int) == ValueTypes.Sayi {
            cell.value.keyboardType = UIKeyboardType.NumberPad
        } else if ValueTypes(rawValue: self.detailTemplates[indexPath.row]["type"] as! Int) == ValueTypes.Para {
            cell.value.keyboardType = UIKeyboardType.DecimalPad
        }
        
        if indexPath.row == 0 {
            cell.value.becomeFirstResponder()
        }
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.detailTemplates.count
    }
    
}

extension AddNewDetailViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}