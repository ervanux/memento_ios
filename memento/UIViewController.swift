//
//  UIViewController.swift
//  memento
//
//  Created by Erkan Ugurlu on 04/10/15.
//  Copyright © 2015 LameCoders. All rights reserved.
//

import UIKit
import Parse

extension UIViewController {
    
    func showErrorPopup(message:String){
        let alert = UIAlertController(title: "Bi sıkıntı var", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.Cancel, handler: {(action) -> Void in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func isStringValid(string:String!) -> Bool {
        if string != nil && !string.isEmpty {
            return true
        } else {
            return false
        }
    }
    
    func showLoading(){
        DibaLoader.show(title: nil)
    }
    
    func hideLoading(){
        DibaLoader.hide()
    }
    
    func findObjects(root:PFObject, relationKey:String, completion: [PFObject]?->Void ){
        self.showLoading()
        root.relationForKey(relationKey).query()?
            //        .fromLocalDatastore()
            .orderByDescending("createdAt")
            .findObjectsInBackgroundWithBlock {
                (objects: [PFObject]?, error: NSError?) -> Void in
                if error == nil {
                    completion(objects)
                } else {
                    // Log details of the failure
                    if error!.code == 209 {
                        self.navigationController?.popToRootViewControllerAnimated(false)
                    } else {
                        log.info("Error: \(error!) \(error!.userInfo)")
                        self.showErrorPopup("Verileri çekemedim")
                    }
                    
                }
                self.hideLoading()
        }
    }
    
    
    
    func findObjectsWithPointer(root:PFObject, pointerKey:String, className:String, completion: [PFObject]?->Void ){
        self.showLoading()
        let query = PFQuery(className:className)
        query.whereKey(pointerKey, equalTo: root)
        query.orderByDescending("createdAt")
        query.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
                if error == nil {
                    completion(objects)
                } else {
                    // Log details of the failure
                    if error!.code == 209 {
                        self.navigationController?.popToRootViewControllerAnimated(false)
                    } else {
                        log.info("Error: \(error!) \(error!.userInfo)")
                        self.showErrorPopup("Verileri çekemedim")
                    }
                    
                }
                self.hideLoading()
        }
    }

}

