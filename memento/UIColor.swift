//
//  File.swift
//  memento
//
//  Created by Erkan Ugurlu on 14/07/15.
//  Copyright (c) 2015 LameCoders. All rights reserved.
//

import UIKit

extension UIColor {
    
    var hexString:NSString {
        let colorRef = CGColorGetComponents(self.CGColor)
        
        let r:CGFloat = colorRef[0]
        let g:CGFloat = colorRef[1]
        let b:CGFloat = colorRef[2]
        
        return NSString(format: "#%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(_ netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    convenience init(hexString:String) {
        let hexString:NSString = hexString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let scanner            = NSScanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
}

let array = [
        0xfb0a2a,
        0x02adea,
        0x00405d,
        0xffcc33,
        0xff6138,
        0xff0000,
        0xfcd20b,
        0xe47911,
        0xa4c639,
        0x1d8dd5,
        0x003366,
        0x005cff,
        0xee6123,
        0xfc4f08,
        0x613854,
        0xff7243,
        0xb50900,
        0x3d4944,
        0x205cc0,
        0x3287c1,
        0xe54a4f,
        0x4e6252,
        0x2d72da,
        0xfebd17,
        0x59a3fc,
        0xdb7132,
        0xea4c89,
        0x3d9ae8,
        0x0c76ab,
        0x2a323a,
        0x89c507,
        0xf05e1b,
        0x00bdf6,
        0x528036,
        0xeb6d20,
        0x5ba525,
        0xdd0017,
        0x3b5998,
        0xe66000,
        0x0063dc,
        0xff0084,
        0x5b9a68,
        0x25a0ca,
        0x007cc3,
        0x2d75a2,
        0xf70078,
        0x171515,
        0x0140ca,
        0x16a61e,
        0xdd1812,
        0xfcca03,
        0xdd4b39,
        0xf77f00,
        0x82b548,
        0xff6600,
        0x0085ca,
        0xc7c5e6,
        0x6567a5,
        0x003366,
        0x73ba37,
        0x0096d6,
        0xec6231,
        0x8cc83b,
        0x003e6a,
        0xffcc33,
        0xf3ce13,
        0x3f729b,
        0x1c1c1c,
        0x0071c5,
        0x365ebf,
        0x76cc1e,
        0xe03500,
        0x00af81,
        0xc3000d,
        0x0e76a8,
        0xcf0005,
        0x576396,
        0xd82028,
        0x29a0b7,
        0xa086d3,
        0xe51937,
        0xb9070a,
        0x183693,
        0x76b900,
        0xed812b,
        0xcc0f16,
        0xe41f11,
        0x1e477a,
        0x3b7bbf,
        0x0000e6,
        0xc8232c,
        0x665cbe,
        0xee4056,
        0x318bff,
        0x0f71b4,
        0xa82400,
        0x66ceff,
        0x008fd5,
        0x9c0000,
        0xcc0000,
        0xcee2f8,
        0xff4500,
        0x7eb400,
        0x0ba6ab,
        0x62b0d9,
        0xee802f,
        0x1798c1,
        0x0c4da2,
        0x96bf48,
        0x00aff0,
        0xf0503a,
        0xf47a20,
        0x008ace,
        0xff7700,
        0xf86960,
        0x81b71a,
        0xfee100,
        0x121212,
        0xef8236,
        0xcc0000,
        0xd7584f,
        0x008cdd,
        0x00afe1,
        0xf74425,
        0xea0a8e,
        0x40a800,
        0xef4423,
        0x5cb868,
        0x256a92,
        0x5eab1f,
        0x34526f,
        0x6441a5,
        0x00acee,
        0x9aca3c,
        0xff8700,
        0xdd4814,
        0x3388ff,
        0x3d95ce,
        0xef1d1d,
        0x44bbff,
        0x00a478,
        0x06afd8,
        0xcc0000,
        0x45668e,
        0x5b009c,
        0x21759b,
        0xd54e21,
        0x464646,
        0x2b88d9,
        0x9bc848,
        0x126567,
        0x720e9e,
        0xffcc00,
        0xc41200,
        0xc4302b,
        0x5498dc,
        0x78a300,
        0x9dcc7a,
        0x5e8b1d
]
