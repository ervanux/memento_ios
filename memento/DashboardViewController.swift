//
//  DashboardViewController.swift
//  memento
//
//  Created by Erkan Ugurlu on 12/07/15.
//  Copyright (c) 2015 LameCoders. All rights reserved.
//

import UIKit
import Parse

class AddButton:UIButton{
    var indexPath:NSIndexPath!
}

class CustomCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var addBtn: AddButton!
    
    
}

class DashboardViewController: UIViewController {
    
    var array=[PFObject]()
    var objectsFetched = false
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        // Do any additional setup after loading the view.
        self.tableView.layer.cornerRadius = 5.0
        //        self.pinAll()
        self.reloadDatas()
    }
    
    //    func pinAll(){
    //        let query = PFQuery(className:"Category")
    //        query.whereKey("user", equalTo: PFUser.currentUser()!)
    //
    //        query.findObjectsInBackgroundWithBlock { (categories:[PFObject]?, error:NSError?) -> Void in
    //            self.hideLoading()
    //            if let error = error  {
    //                if error.code == 209 {
    //                    self.navigationController?.popViewControllerAnimated(false)
    //                } else {
    //                    self.reloadDatas()
    //                }
    //            } else {
    //                PFObject.pinAllInBackground(categories)
    //                if let categories = categories {
    //                    let eventQuery = PFQuery(className:"Event")
    //                    eventQuery.whereKey("category", containedIn: categories)
    //                    eventQuery.findObjectsInBackgroundWithBlock({ (events:[PFObject]?, error:NSError?) -> Void in
    //                        PFObject.pinAllInBackground(events)
    //
    //                        if let events = events {
    //                            let eventQuery = PFQuery(className:"EventDetail")
    //                            eventQuery.whereKey("event", containedIn: events)
    //                            eventQuery.findObjectsInBackgroundWithBlock({ (details:[PFObject]?, error:NSError?) -> Void in
    //                                PFObject.pinAllInBackground(details)
    //                            })
    //                        }
    //
    //                    })
    //
    //                    let templateQuery = PFQuery(className:"EventDetailTemplate")
    //                    templateQuery.whereKey("category", containedIn: categories)
    //                    templateQuery.findObjectsInBackgroundWithBlock({ (templates:[PFObject]?, error:NSError?) -> Void in
    //                        PFObject.pinAllInBackground(templates)
    //                    })
    //
    //
    //                }
    //                self.reloadDatas()
    //            }
    //        }
    //    }
    
    func reloadDatas (){
        self.findObjectsWithPointer(PFUser.currentUser()!, pointerKey: "user", className: "Category",
//        self.findObjects(PFUser.currentUser()!, relationKey: "categories",
            completion: { (categories:[PFObject]?) -> Void in
                self.objectsFetched = true
                if let objects = categories {
                self.array.removeAll(keepCapacity: false)
                for object in objects {
                    self.array.append(object)
                }
                self.tableView.reloadData()
            }
        })
        
    }
    
    @IBAction func pressedLogoutButton(sender: AnyObject) {
        self.logout()
    }
    
    func logout(){
        self.showLoading()
        PFUser.logOutInBackgroundWithBlock { (error) -> Void in
            self.hideLoading()
            self.performSegueWithIdentifier("logout", sender: nil)
        }
        
    }
    
    @IBAction func unwindFromAdding(sende:UIStoryboardSegue){
        self.reloadDatas()
    }
    
    @IBAction func unwindFromAddingWithClose(sende:UIStoryboardSegue){
        
    }
    
    
    @IBAction func unwindFromDetailAdding(sende:UIStoryboardSegue){
        self.reloadDatas()
    }
    
    @IBAction func unwindFromDetailDeleted(sende:UIStoryboardSegue){
        self.reloadDatas()
    }
    
    @IBAction func pressedEditButton(sender: AnyObject) {
        self.tableView.setEditing(!self.tableView.editing, animated: true)
    }
}


extension DashboardViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if self.array.count > 0 {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! CustomCell
        let object = self.array[indexPath.row]
        
        cell.title?.text = object["name"] as? String
        
        if let color = object["colorCode"] as? String {
            cell.backgroundColor = UIColor(hexString:color)
        }
        cell.addBtn.indexPath = indexPath
        cell.addBtn.addTarget(self, action: "pressedAddBtn:", forControlEvents: UIControlEvents.TouchUpInside)
        
        return cell
        } else {
            return tableView.dequeueReusableCellWithIdentifier("Cell2") as! CustomCell
        }
    }
    
    func pressedAddBtn(sender:AddButton){
        let category = self.array[sender.indexPath.row]
        self.performSegueWithIdentifier("addNewDetail", sender: category)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !self.objectsFetched {
          return 0
        } else if self.array.count > 0 {
            return self.array.count
        } else {
            return 1
        }

    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        if tableView.editing {
            return true
        } else {
            return false
        }
        
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        // you need to implement this method too or you can't swipe to display the actions
        if editingStyle == UITableViewCellEditingStyle.Delete{
            self.showLoading()
            self.tableView.editing = false

            
            self.findObjects(self.array[indexPath.row], relationKey: "events", completion: { (events:[PFObject]?) -> Void in                
                    if let events = events {
                        for event in events {
                            self.findObjects(event, relationKey: "details", completion: { (details:[PFObject]?) -> Void in
                                if let details = details {
                                    PFObject.deleteAllInBackground(details)
                                }
                            })
                        }
                        PFObject.deleteAllInBackground(events)
                    }
                })

            
            self.findObjects(self.array[indexPath.row], relationKey: "detailTemplates", completion: { (objects:[PFObject]?) -> Void in
                    PFObject.deleteAllInBackground(objects)
                })
            
            self.array[indexPath.row].deleteInBackgroundWithBlock({ (result, error) -> Void in
                if result {
                    self.reloadDatas()
                } else {
                    self.showErrorPopup("Silemedim")
                }
            })
            
            
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        self.performSegueWithIdentifier("showDetails", sender: self.array[indexPath.row])
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addNewDetail" {
            let dest = segue.destinationViewController.childViewControllers[0] as! AddNewDetailViewController
            if let sender = sender as? PFObject {
                dest.category = sender
            }
        } else if segue.identifier == "showDetails" {
            let dest = segue.destinationViewController as! ListEventAndDetailViewController
            dest.category = sender as! PFObject
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        
    }
    
}

