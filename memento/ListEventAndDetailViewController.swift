//
//  ListEventAndDetailViewController.swift
//  memento
//
//  Created by Erkan Ugurlu on 09/08/15.
//  Copyright (c) 2015 LameCoders. All rights reserved.
//

import UIKit
import Parse

class CustomDetailCell : UITableViewCell {
    @IBOutlet weak var name:UILabel!
    @IBOutlet weak var value:UILabel!
    @IBOutlet weak var backView: UIView!
}

class CustomDetailHeaderCell : UITableViewCell {
    @IBOutlet weak var label:UILabel!
    @IBOutlet weak var backView: UIView!
}

class ListEventAndDetailViewController: UIViewController {
    var objectsFetched = false
    
    @IBOutlet weak var tableView: UITableView!
    var category:PFObject!
    var events = [PFObject]()
    var eventDetails = [PFObject]()
    var eventDetailTemplates = [PFObject]()
    var finishedTaskCounter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = self.category["name"] as? String
        
        //        loadWithPointer()
        loadWithRelation()
        
    }
    
    func loadWithRelation(){
        
        self.findObjects(self.category!, relationKey: "events", completion: { (events:[PFObject]?) -> Void in
            if let events = events where events.count>0 {
                self.events = events
            }
            self.loadTableView()
            
        })
        
        
        self.findObjects(self.category, relationKey: "detailTemplates", completion: { (templates:[PFObject]?) -> Void in
            if let templates = templates where templates.count>0 {
                self.eventDetailTemplates = templates
            }
            self.loadTableView()
        })
    }
    
    
    func loadTableView() {
        if ++finishedTaskCounter == 2 {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.objectsFetched = true
                self.tableView.reloadData()
                //                self.hideLoading()
            })
        }
    }
}

extension ListEventAndDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if self.events.count > 0 {
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier("Cell0") as! CustomDetailHeaderCell
                cell.label.text = dateformatterDate(self.events[indexPath.section].createdAt!) as String
                
                if let color = self.category["colorCode"] as? String {
                    cell.backView.backgroundColor = UIColor(hexString:color)
                }
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCellWithIdentifier("Cell1") as! CustomDetailCell
                let temp = self.eventDetailTemplates[indexPath.row-1]
                cell.name.text = temp["name"] as? String
                
                self.findObjects(self.events[indexPath.section], relationKey: "details", completion: { (eventDetails:[PFObject]?) -> Void in
                    
                    if let eventDetails = eventDetails where eventDetails.count>0 {
                        for detail in eventDetails {
                            if let tid = temp.objectId, tempId =  detail["template"].objectId where tid == tempId  {
                                cell.value.text = detail["value"] as? String
                                break
                            }
                            
                        }}
                    
                })
                
                
                if let color = self.category["colorCode"] as? String {
                    cell.backView.backgroundColor = UIColor(hexString:color)
                }
                
                return cell
            }
        } else {
            return tableView.dequeueReusableCellWithIdentifier("Cell2")!
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.events.count > 0 {
            return 15
        } else  {
            return 0.01
        }
        
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if self.events.count > 0 {
            return 15
        } else  {
            return 0.01
        }
        
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.events.count > 0 {
            let viewback = UIView()
            let viewh = UIView(frame: CGRect(x: 20, y: 10, width: tableView.bounds.size.width-40, height: 5))
            viewh.backgroundColor = UIColor(hexString:self.category["colorCode"] as! String)
            viewh.setMask([.TopRight,.TopLeft])
            viewback.addSubview(viewh)
            return viewback
        } else  {
            return nil
        }
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if self.events.count > 0 {
            let viewback = UIView()
            let viewh = UIView(frame: CGRect(x: 20, y: 0, width: tableView.bounds.size.width-40, height: 5))
            viewh.backgroundColor = UIColor(hexString:self.category["colorCode"] as! String)
            viewh.setMask([.BottomLeft,.BottomRight])
            viewback.addSubview(viewh)
            return viewback
        } else  {
            return nil
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !self.objectsFetched {
            return 0
        } else if self.events.count > 0 {
            return self.eventDetailTemplates.count+1
        } else {
            return 1
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if !self.objectsFetched {
            return 0
        } else if self.events.count > 0 {
            return self.events.count
        } else {
            return 1
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
}

//    func loadWithPointer(){
//        let eventQuery = PFQuery(className: "Event")
//        eventQuery.whereKey("category", equalTo: category)
////            .fromLocalDatastore()
//        eventQuery.orderByDescending("createdAt")
//        //        showLoading()
//
//        eventQuery.findObjectsInBackgroundWithBlock { (list:[PFObject]?, error:NSError?) -> Void in
//            if let list = list {
//                for i in list {
//                    //                    print(i, terminator: "")
//                    self.events.append(i)
//                }
//            }
//            self.loadTableView()
//        }
//
//        let eventDetailTemplateQuery = PFQuery(className: "EventDetailTemplate")
//        eventDetailTemplateQuery.whereKey("category", equalTo: category)
////            .fromLocalDatastore()
//        eventDetailTemplateQuery.findObjectsInBackgroundWithBlock {
//            (list: [PFObject]?, error: NSError?) -> Void in
//            if let list = list {
//                for i in list {
//                    //                    print(i, terminator: "")
//                    self.eventDetailTemplates.append(i)
//                }
//            }
//            self.loadTableView()
//        }
//    }
//

